
## **Team Radian** 😎

- Rohan Babbar - rohanbabbar0408@gmail.com

- Manu Dev - manudev0004@gmail.com

- Shivam Kumar - shivamkumardss2018@gmail.com

- Abhinav Kumar - dead.ground.75000@gmail.com

# Theme 👇
## Filter and categorise Youtube videos


# Solution to our Problem👨‍💻✔️
We're going to use Python and SK-learn to create an AI model. We will use different YouTube video data to train them and predict whether the video is educative or not. Further we can make a website where we will use this in the back-end to predict a video. User can enter the URL to see the output.

# Our Final Demo 👨‍🔬

We will make a user-friendly website with good looking UI. The user will enter video url to see the result predicted using our AI model.


